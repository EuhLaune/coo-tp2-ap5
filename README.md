# COO-TP2-AP5


Les réponses sont dans les messages de commits.
Pour les lire:

```shell
git log
```

Pour essayer un exercice à la fois:

```shell
git checkout <commit hash>
```

L'UML est le suivant:

![alt text](UML.png "Un UML de qualité, pour un professeur de qualité.")

Merci.
