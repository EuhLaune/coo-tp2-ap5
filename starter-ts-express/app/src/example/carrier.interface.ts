import { Contact1 } from './contact.interface'

interface Carrier {
    name   : string;
    contact: Contact1;
}

export default Carrier
