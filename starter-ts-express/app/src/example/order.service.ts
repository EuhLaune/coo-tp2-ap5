import IOrder from './order.interface'
import IService from "./service.interface";
import Order from "./order.model";

export class OrderService implements IService {

    private model : Order = new Order('orders');

    public delete = async (id: number) : Promise<boolean> => {
        const cmdRedis : string  = await this.model.getDatas();
        const cmd      : IOrder[] = JSON.parse(cmdRedis) || [];
        const cmdDel   : IOrder = cmd.find((order) => order.id === id);

        if (!cmdDel)
            return false;

        const cmdMaj: IOrder[] = cmd.filter((order) => order.id !== cmdDel.id);
        await this.model.setData(cmdMaj);
        return true;
    };

    public deleteAll = async () : Promise<boolean> => {
        await this.model.delDatas();
        return true;
    };

    public get = async (id: number) : Promise<IOrder> => {
        const cmdRedis : string  = await this.model.getDatas();
        const cmd      : IOrder[] = JSON.parse(cmdRedis) || [];

        return cmd.find((order) => order.id === id);
    };

    public getAll = async () : Promise<IOrder[]> => {
        return JSON.parse(await this.model.getDatas());
    }

    public put = async (id : number, majInfo : any) : Promise<boolean> => {
        const cmdRedis : string   = await this.model.getDatas();
        const cmds     : IOrder[] = JSON.parse(cmdRedis) || [];
        const cmdMaj   : IOrder = cmds.find((order: IOrder) => order.id === id);

        if (!cmdMaj)
            return false;

        const cmdUpdated = {
            ...cmdMaj,
            ...majInfo,
        };

        const newCmdsTab = cmds.map((order: IOrder) => order.id === cmdUpdated.id ? cmdUpdated : order);
        await this.model.setData(newCmdsTab);
        return true;
    }

    public create = async (cmd : any) : Promise<IOrder> => {
        const cmdRedis   : string  = await this.model.getDatas();
        const cmds       : IOrder[] = JSON.parse(cmdRedis) || [];
        const cmdsTriees : IOrder[] = cmds.sort((precedent : IOrder, courant : IOrder) => {
            return courant.id - precedent.id
        });
        const dernierId   : number = cmds.length > 0 ? cmdsTriees[0].id : 0;
        cmd = {
            id: dernierId + 1,
            createdAt: new Date(),
            ...cmd,
        };

        const newCmdsTab: IOrder[] = [...cmds, cmd];
        await this.model.setData(newCmdsTab);
        return cmd;


    }
}
