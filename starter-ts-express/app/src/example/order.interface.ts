import Package from './package.interface'
import {Contact2} from './contact.interface'
import Carrier from './carrier.interface'

interface IOrder {
    id       : number;
    createdAt: Date;
    packages : Package[];
    contact  : Contact2;
    carrier  : Carrier;
}

export default IOrder
