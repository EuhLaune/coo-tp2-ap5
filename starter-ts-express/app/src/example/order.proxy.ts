import IOrder from './order.interface';
import {OrderService} from './order.service';
import IService from "./service.interface";

export class OrderProxy implements IService {

    private _service : IService;

    constructor(service: IService) {
        this._service = service;
    }

    async create(cmd: any): Promise<IOrder> {
        return this._service.create(cmd);
    }

    async delete(id: number): Promise<boolean> {
        return this._service.delete(id);
    }

    async deleteAll(): Promise<boolean> {
        return this._service.deleteAll();
    }

    async get(id: number): Promise<IOrder> {
        return this._service.get(id);
    }

    async getAll (): Promise<IOrder[]> {
        const privateInfoContact1: any = {
                firstname: "****",
                lastname: "****",
                phone: "****",
                mail: "****",
                headOfficeAddress: {
                    postalCode: "****",
                    city: "****",
                    addressLine1: "****",
                    addressLine2: "****"
                }
        };
        const privateInfoContact2: any = {
            firstname: '****',
            lastname: '****',
            phone: '****',
            mail: '*****',
            billingAddress: {
                postalCode: '****',
                city: '****',
                addressLine1: '****',
                addressLine2: '****'
            },
            deliveryAddress: {
                postalCode: '****',
                city: '****',
                addressLine1: '****',
                addressLine2: '****'
            }
        };
        let orderArray: IOrder[] = await this._service.getAll() || [];
        orderArray.forEach(order => {
            order.contact = privateInfoContact2;
            order.carrier.contact = privateInfoContact1;
        });
        return orderArray;
    };

    async put(id: number, majInfo: any): Promise<boolean> {
        return this._service.put(id, majInfo);
    }

}
