import Address from './adress.interface'

interface Contact {
    firstname        : string;
    lastname         : string;
    phone            : string;
    mail             : string;
}

export interface Contact1 extends Contact {
    headOfficeAddress: Address;
}

export interface Contact2 extends Contact {
    billingAddress : Address;
    deliveryAddress: Address;
}
