interface Product {
    quantity: number;
    label   : string;
    ean     : string;
}

export default Product
