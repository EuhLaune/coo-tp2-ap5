import IOrder from "./order.interface";

export default interface IService {
    delete (id: number) : Promise<boolean>

    deleteAll() : Promise<boolean>

    get(id: number) : Promise<IOrder>

    getAll() : Promise<IOrder[]>

    put(id: number, majInfo: any): Promise<boolean>

    create(cmd : any) : Promise<IOrder>
}