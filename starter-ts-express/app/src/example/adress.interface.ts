interface Address {
    postalCode : number;
    city       : string;
    adressLine1: string;
    adressLine2: string;
}

export default Address
