import {
    delAsync,
    getAsync,
    setAsync,
} from '../../utils/storage';
import IOrder from './order.interface';

export default class Order {

    private base: string;

    constructor(base: string) {
        this.base = base;
    }

    public getDatas = async () : Promise<string> => {
        return getAsync(this.base);
    };

    public setData = async (data: IOrder[]) => {
        setAsync(this.base, JSON.stringify(data));
    };

    public delDatas = async () => {
        delAsync(this.base);
    };
}
