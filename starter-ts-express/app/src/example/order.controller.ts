/* tslint:disable:typedef-whitespace semicolon */
import {
    Request,
    Response,
    Router,
} from 'express'

import IOrder from './order.interface'

import {OrderProxy} from "./order.proxy";
import IService from "./service.interface";
import {OrderService} from "./order.service";

export default class OrderController {
    public  PATH    = '/orders';
    public  PATH_ID = this.PATH + '/:id';
    public  router  = Router();
    private service : IService = new OrderProxy(new OrderService());

    constructor() {
        this.intializeRoutes()
    }

    public intializeRoutes() {
        // Requête /orders
        this.router.get(this.PATH, this.getAll);
        this.router.get(this.PATH_ID, this.get);

        this.router.put(this.PATH_ID, this.put);

        this.router.post(this.PATH, this.create);

        this.router.delete(this.PATH, this.deleteAll);
        this.router.delete(this.PATH_ID, this.delete)
    }

    public delete = async (request: Request, response: Response) => {
        const id         : number = Number(request.params.id);
        const statusCode : number = await this.service.delete(id) ? 204 : 404;
        response.sendStatus(statusCode);
    };

    public deleteAll = async (request: Request, response: Response) => {
        await this.service.deleteAll()
        response.sendStatus(200);
    };

    // Récupère toutes les commandes de la base de données REDIS
    public getAll = async (request: Request, response: Response) => {
        response.status(200).json(await this.service.getAll());
    };

    // Récupère une commande dans la base de données REDIS
    public get = async (request: Request, response: Response) => {
        const id             : number = Number(request.params.id);
        const order          : IOrder  = await this.service.get(id);
        const nbStatus       : number =  order === null ? 404 : 200;
        response.status(nbStatus).json(order)
    };

    // Modifie une commande dans la base de données REDIS
    public put = async (request: Request, response: Response) => {
        const majInfo            : any     = request.body;
        const id                 : number  = Number(request.params.id);
        response.sendStatus(await this.service.put(id, majInfo) ? 204 : 404)
    };

    // Ajoute une commande dans la base de données REDIS 
    public create = async (request: Request, response: Response) => {
        let commande = request.body;
        response.status(201).json(await this.service.create(commande));
    }
}
