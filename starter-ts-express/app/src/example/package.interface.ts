import Mesure from './mesure.interface'
import Product from './product.interface'

interface Package {
    length  : Mesure;
    width   : Mesure;
    height  : Mesure;
    weight  : Mesure;
    products: Product[];
}

export default Package
